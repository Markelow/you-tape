
package AsisstClasses;

import java.util.*;
import AsisstClasses.*;
/**
 *
 * @author Markeli
 */
public class WallData 
{
    private long likesCount;
    private long repostsCount;
    private long commentsCount;
    private ArrayList<PostData> posts;
    
    public long GetLikesCount()
    {
        return likesCount;
    }
    
    public void SetLikesCount(long likesCount)
    {
        this.likesCount = likesCount;
    }
    
    public long GetRepostCount()
    {
        return repostsCount;
    }
    
    public void SetRepostCount(long repostCount)
    {
        this.repostsCount = repostCount;
    }
    
    public long GetCommentCount()
    {
        return commentsCount;
    }
    
    public void SetCommentCount(long commentsCount)
    {
        this.commentsCount = commentsCount;
    }
    
    public ArrayList<PostData> GetPosts()
    {
        return posts;
    }
    
    public void SetPosts(ArrayList<PostData> posts)
    {
        this.posts = posts;
    }
    
    public void AddPost(PostData postData)
    {
        posts.add(postData);
        likesCount += postData.GetLikesCount();
        repostsCount += postData.GetRepostCount();
        commentsCount += postData.GetCommentCount();
    }
    
    public PostData GetPost(int index)
    {
        return posts.get(index);
    }
    
    public WallData()
    {
        likesCount = 0;
        repostsCount = 0;
        commentsCount = 0;
        posts = new ArrayList();        
    }
}
