package AsisstClasses;

import java.util.*;
/**
 *
 * @author Markeli
 */
public class PostData 
{
    private long likesCount;
    private long repostCount;
    private long commentsCount;
    private Date dateOfPublication;
    
    public long GetLikesCount()
    {
        return likesCount;
    }
    
    public void SetLikesCount(long likesCount)
    {
        this.likesCount = likesCount;
    }
    
    public long GetRepostCount()
    {
        return repostCount;
    }
    
    public void SetRepostCount(long repostCount)
    {
        this.repostCount = repostCount;
    }
    
    public long GetCommentCount()
    {
        return commentsCount;
    }
    
    public void SetCommentCount(long commentsCount)
    {
        this.commentsCount = commentsCount;
    }
    
    public Date GetDateOfPublication()
    {
        return dateOfPublication;
    }
    
    public void SetDateOfPublication(Date dateOfPublication)
    {
        this.dateOfPublication = dateOfPublication;
    }
    
    public PostData()
    {
        likesCount = 0;
        repostCount = 0;
        commentsCount = 0;
        dateOfPublication = new Date();        
    }
}
