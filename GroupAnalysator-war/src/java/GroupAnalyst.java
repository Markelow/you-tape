/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;
import java.net.*;
import java.net.MalformedURLException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.*;
import AsisstClasses.*;
import com.google.gson.Gson;
import java.util.*;

public class GroupAnalyst extends HttpServlet {

    private static final String baseVKURL = "http://api.vk.com/method/";
    private static final String groupURL = "groups.getById?group_id=";
    private static final String groupMembersURL = "groups.getMembers?group_id=";
    private static final String wallURL = "wall.get?owner_id=-";
   // private static final String photosURL = "photos.getAll?owner_id=-";
    private static final String zeroCountURL = "&count=0";
    private static final String oneElemURL = "&count=1";
    private static final String countOfElemsURL = "&count=";
    private static final String APIVersionURL = "&v=5.21";
    private static final String offsetURL = "&offset=";
    private static final int maxDay = -3;
    private static final int countOfElems = 10;
    
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        PrintWriter out = response.getWriter();
        try 
        {
            response.setContentType("text/html;charset=UTF-8");
            String groupId = request.getParameter("groupId");
            String correctGroupId;
            correctGroupId = Long.toString(GetCorrectGroupId(groupId));
            out.println("<p>Group Id: " + correctGroupId + "</p>");
            out.println("<p>Total:</p>");
            out.println("<p>User count: " + GetUsersCount(correctGroupId) +"</p>");
            out.println("<p>Post count: " + GetPostsCount(correctGroupId) +"</p>");
            WallData wallAnalysis = GetWallAnalysis(correctGroupId);
            out.println("<p>Likes count: " + wallAnalysis.GetLikesCount() +"</p>");  
            out.println("<p>Reposts count: " + wallAnalysis.GetRepostCount() +"</p>");                   
            out.println("<p>Comments count: " + wallAnalysis.GetCommentCount() +"</p>");
            if (0 != wallAnalysis.GetPosts().size() )
            {
                out.println("<p>Posts analysis:</p>");
                for (int i=0; i<wallAnalysis.GetPosts().size();++i)
                {
                    out.println("<p>Date of publication: " + wallAnalysis.GetPost(i).GetDateOfPublication() +"</p>");
                    out.println("<p>--Likes count: " + wallAnalysis.GetPost(i).GetLikesCount() +"</p>");  
                    out.println("<p>--Reposts count: " + wallAnalysis.GetPost(i).GetRepostCount() +"</p>");                   
                    out.println("<p>--Comments count: " + wallAnalysis.GetPost(i).GetCommentCount() +"</p>");
                }
            }
        }
        catch(Exception ex)
        {
            out.println(ex.getMessage());
        }
    }
    
    private long GetCorrectGroupId(String groupID) throws Exception
    {
        JSONArray jsonArray =  GetJSONArray(baseVKURL + groupURL + groupID + APIVersionURL);
        return (long)((JSONObject)jsonArray.get(0)).get("id");
    }
    
    private JSONArray GetJSONArray(String requestURL) throws Exception
    {
        URL groupRequest = new URL(requestURL);
        BufferedReader reader = new BufferedReader(new InputStreamReader(groupRequest.openStream()));
        String response = reader.readLine();
        JSONParser parser = new JSONParser();
        JSONObject jsonResponse = (JSONObject)parser.parse(response);
        return (JSONArray)jsonResponse.get("response");
    }
    
    private Object GetUsersCount(String groupId) throws Exception
    {
        try
        {
            return GetJSONObject(baseVKURL+groupMembersURL + groupId + zeroCountURL).get("count");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    private JSONObject GetJSONObject(String requestURL) throws Exception
    {
        URL groupRequest = new URL(requestURL);
        BufferedReader reader = new BufferedReader(new InputStreamReader(groupRequest.openStream()));
        String response = reader.readLine();
        JSONParser parser = new JSONParser();
        JSONObject jsonResponse = (JSONObject)parser.parse(response);
        return (JSONObject)jsonResponse.get("response");
    }
    
    private Object GetPostsCount(String groupId) throws Exception
    {
        try
        {
            return GetJSONObject(baseVKURL+wallURL + groupId + oneElemURL + APIVersionURL).get("count");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    private WallData GetWallAnalysis(String groupId) throws Exception
    {
        Date borderDate = GetBorderDate();
        Date lastDate = new Date();
        int offset = 0;
        WallData wallAnalysis = new WallData();
        do
        {
            String requestURL = baseVKURL+wallURL + groupId + offsetURL + offset*countOfElems + countOfElemsURL + countOfElems + APIVersionURL;
            JSONArray wallItems = (JSONArray)GetJSONObject(requestURL).get("items");
            for (int j=0; j<wallItems.size(); ++j)
            {
                JSONObject item = (JSONObject)wallItems.get(j);
                lastDate = ConvertDateFromUnixTime( (long)item.get("date") );
                if (borderDate.before(lastDate))
                {
                    JSONObject paramObject;
                    PostData postData = new PostData();
                    postData.SetDateOfPublication(lastDate);
                    paramObject = (JSONObject)item.get("likes");
                    postData.SetLikesCount((long)paramObject.get("count"));
                    paramObject = (JSONObject)item.get("reposts");
                    postData.SetRepostCount((long)paramObject.get("count"));
                    paramObject = (JSONObject)item.get("comments");
                    postData.SetCommentCount((long)paramObject.get("count"));
                    wallAnalysis.AddPost(postData);
                }
            }
            offset++;
        }
        while (borderDate.before(lastDate));
        return wallAnalysis;
    }
    
    private Date ConvertDateFromUnixTime(long unixSeconds)
    {
        Date a = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(a);
            c.add(Calendar.DATE, -3);
        return new Date(unixSeconds*1000);
    }
    
    private Date GetBorderDate()
    {
        Date currentDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.DATE, maxDay);
        return calendar.getTime();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() 
    {
        return "Short description";
    }

}
